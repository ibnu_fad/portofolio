-- phpMyAdmin SQL Dump
-- version 3.2.0.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 18, 2017 at 03:11 AM
-- Server version: 5.1.37
-- PHP Version: 5.3.0

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `online_shop`
--

-- --------------------------------------------------------

--
-- Table structure for table `table_cat`
--

CREATE TABLE IF NOT EXISTS `table_cat` (
  `no` int(11) NOT NULL AUTO_INCREMENT,
  `kategori` varchar(120) NOT NULL,
  PRIMARY KEY (`no`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `table_cat`
--

INSERT INTO `table_cat` (`no`, `kategori`) VALUES
(1, 'pantai'),
(2, 'gunung'),
(3, 'diving');

-- --------------------------------------------------------

--
-- Table structure for table `table_categori`
--

CREATE TABLE IF NOT EXISTS `table_categori` (
  `no` int(10) NOT NULL AUTO_INCREMENT,
  `kategori` varchar(50) NOT NULL,
  `file` text NOT NULL,
  PRIMARY KEY (`no`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `table_categori`
--

INSERT INTO `table_categori` (`no`, `kategori`, `file`) VALUES
(1, 'wisata', ''),
(2, 'kuliner', ''),
(3, 'hotel', '');

-- --------------------------------------------------------

--
-- Table structure for table `table_isi`
--

CREATE TABLE IF NOT EXISTS `table_isi` (
  `no` int(11) NOT NULL AUTO_INCREMENT,
  `kategori` varchar(100) NOT NULL,
  `isi` text NOT NULL,
  `file` text NOT NULL,
  `namalink` varchar(100) NOT NULL,
  PRIMARY KEY (`no`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `table_isi`
--

INSERT INTO `table_isi` (`no`, `kategori`, `isi`, `file`, `namalink`) VALUES
(1, 'darat', 'kerata itu gapernah kena macet juga ga pernah ketilang dan ga ada lampi merahnya', 'asd.jpg', 'kereta'),
(2, 'darat', 'mobil yang bisa terbangg', 'mobil.jpg', 'mobil lampard'),
(3, 'laut', 'di laut ada ikaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaannnnnnnnnn ya ? :v', 'laut.jpg', 'kapal selam'),
(4, 'laut', 'kapal miring kapten kapal kita diinvasi oleh mpok ijahh bahaya persediaan oksigen menispis kapten', 'ase.jpg', 'pesiar'),
(5, 'udara', 'satu satunya tempat dimana tidak ada kemacetan dan tidak ad orang yang ramai lalu lang menggunakan kendaran bermotor', 'jepang.jpg', 'kapal terbang'),
(6, 'udara', 'diudara itu kita tidak menapak dengang tanah melainkan kita melayang layang', 'layang.jpg', 'pesawat jett');

-- --------------------------------------------------------

--
-- Table structure for table `table_konten`
--

CREATE TABLE IF NOT EXISTS `table_konten` (
  `no` int(10) NOT NULL AUTO_INCREMENT,
  `kategori` varchar(50) NOT NULL,
  `file` text NOT NULL,
  `isi` text NOT NULL,
  `namalink` varchar(50) NOT NULL,
  PRIMARY KEY (`no`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `table_konten`
--

INSERT INTO `table_konten` (`no`, `kategori`, `file`, `isi`, `namalink`) VALUES
(1, 'wisata', 'asd.jpg', 'wisata ke puncak mantappp udaranya sugerrrrrrrrr', 'puncak'),
(2, 'wisata', 'bunaken.jpg', 'dibunaken ini tempatnya sangat bagus untuk snorkling karena terumbu karangnya sangat indahhh mantap', 'bunaken'),
(3, 'kuliner', 'jos.jpg', 'tahu goreng', 'dibulat dadakan'),
(4, 'kuliner', 'su.jpg', 'pancen oye', 'parah men'),
(5, 'hotel', 'rumah.jpg', 'hotel murah tapi kualitas tidak mengecewakan mantap murah sekalii', 'hotel dah ***'),
(6, 'hotel ', 'puncak.jpg', 'hotel ini menyediakan sarapan dipagi hari dengan menu yang greget seperti granat bakar , sayur sop dengkul t-rex dan lain-lain jangan tanya siapa yang masak', 'hotel bang greget'),
(7, 'hotel', '', 's', 'ss');

-- --------------------------------------------------------

--
-- Table structure for table `table_login`
--

CREATE TABLE IF NOT EXISTS `table_login` (
  `no` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  PRIMARY KEY (`no`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `table_login`
--

INSERT INTO `table_login` (`no`, `user`, `password`) VALUES
(1, 'admin', '12345');

-- --------------------------------------------------------

--
-- Table structure for table `table_rec`
--

CREATE TABLE IF NOT EXISTS `table_rec` (
  `no` int(11) NOT NULL AUTO_INCREMENT,
  `kategori` varchar(120) NOT NULL,
  `isi` text NOT NULL,
  `file` text NOT NULL,
  `namalink` varchar(120) NOT NULL,
  PRIMARY KEY (`no`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `table_rec`
--

INSERT INTO `table_rec` (`no`, `kategori`, `isi`, `file`, `namalink`) VALUES
(1, 'pantai', 'pantai pasir putih memiliki pasir yg berwarna putih dan air laut yang terasa asin jika anda minum jika anda masuk air tapi anda tidak bisa berenang dijamin anda akan tenggelam ', 'pantai.jpg', 'pantai pasir putih'),
(2, 'gunung', 'gunung bromo adalah tempat yang sangat cocok untuk para pendaki dan dipuncak bromo juga terdapat pasir konon katanya disana terdapat pintu ke alam gaib ', 'bromo.jpg', 'gunung bromo'),
(3, 'diving ', 'jika anda ingin diving kami menyaran kan anda untuk memilih tempat bunaken, raja ampat ,maladewa sesuaikan dengan budget anda jika anda ingin diving pastikan disana ada airnya', 'diving.jpg', 'raja ampat');

-- --------------------------------------------------------

--
-- Table structure for table `table_trans`
--

CREATE TABLE IF NOT EXISTS `table_trans` (
  `no` int(11) NOT NULL AUTO_INCREMENT,
  `kategori` varchar(100) NOT NULL,
  `file` text NOT NULL,
  PRIMARY KEY (`no`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `table_trans`
--

INSERT INTO `table_trans` (`no`, `kategori`, `file`) VALUES
(1, 'darat', ''),
(2, 'laut', ''),
(3, 'udara', '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
